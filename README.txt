-------------------------------------------------------------------------------
Simple word link for Drupal 7.x
-------------------------------------------------------------------------------
Description:
The module provides a filter for text. The filter compares the words from
populated table and makes them as link. Link words is stored in the
database.

Note: Do not forget to include a filter in text format!
